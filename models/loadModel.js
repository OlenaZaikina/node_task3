const mongoose = require('mongoose');
const {Schema} = mongoose;

const loadSchema = new Schema({
  created_by: {
    type: String,
  },
  assigned_to: {
    type: String,
    default: '',
  },
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    default: 'NEW',
  },
  state: {
    type: String,
    enum: ['En route to Pick Up', 'Arrived to Pick Up',
      'En route to delivery', 'Arrived to delivery'],
    default: 'En route to Pick Up',
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: Object,
    required: true,
    properties: {
      'width': Number,
      'length': Number,
      'height': Number,
    },
  },
  logs: {
    type: Array,
    properties: {
      'message': String,
      'time': {
        type: String,
        default: new Date().toISOString().split('.')[0] + 'Z',
      },
    },
  },
  createdDate: {
    type: String,
    default: new Date().toISOString().split('.')[0] + 'Z',
  },
});

module.exports.Load = mongoose.model('Load', loadSchema);
