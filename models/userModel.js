const mongoose = require('mongoose');
const {Schema} = mongoose;

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
    uniq: true,
  },
  createdDate: {
    type: String,
    default: new Date().toISOString().split('.')[0] + 'Z',
  },
});

module.exports.User = mongoose.model('User', userSchema);
