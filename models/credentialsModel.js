const mongoose = require('mongoose');
const {Schema} = mongoose;

const credentialsSchema = new Schema({
  email: {
    type: String,
    required: true,
    uniq: true,
  },
  password: {
    type: String,
    required: true,
  },
});

module.exports.Credentials = mongoose.model('Credentials', credentialsSchema);
