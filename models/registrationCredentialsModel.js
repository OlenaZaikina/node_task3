const mongoose = require('mongoose');
const {Schema} = mongoose;

const registrationCredentialsSchema = new Schema({
  email: {
    type: String,
    required: true,
    uniq: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    enum: ['SHIPPER', 'DRIVER'],
    default: 'SHIPPER',
    required: true,
  },
});

module.exports.RegistrationCredentials = mongoose.model(
    'registrationCredentials', registrationCredentialsSchema);
