const mongoose = require('mongoose');
const {Schema} = mongoose;

const truckSchema = new Schema({
  created_by: {
    type: String,
  },
  assigned_to: {
    type: String,
    default: '',
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    default: 'SPRINTER',
    required: true,
  },
  status: {
    type: String,
    enum: ['OL', 'IS'],
    default: 'IS',
  },
  createdDate: {
    type: String,
    default: new Date().toISOString().split('.')[0] + 'Z',
  },
});

module.exports.Truck = mongoose.model('Truck', truckSchema);
