const express = require('express');
const router = new express.Router();

const {asyncHelper} = require('./helper');
const {authorizationMiddleware} = require('./middlewares/authMiddleware');
const {createLoadContloller, readLoadByIdContloller, readLoadsContloller,
  updateLoadByIdContloller, deleteLoadByIdContloller, postLoadByIdContloller,
  readShipingInfoByIdContloller, readActiveLoadContloller,
  nextLoadStateContloller} = require(
    '../controller/loadController');
router.post('/loads', authorizationMiddleware, asyncHelper(
    createLoadContloller));
router.get('/loads/active', authorizationMiddleware, asyncHelper(
    readActiveLoadContloller));
router.patch('/loads/active/state', authorizationMiddleware, asyncHelper(
    nextLoadStateContloller));
router.get('/loads/:id', authorizationMiddleware, asyncHelper(
    readLoadByIdContloller));
router.get('/loads', authorizationMiddleware, asyncHelper(
    readLoadsContloller));
router.put('/loads/:id', authorizationMiddleware, asyncHelper(
    updateLoadByIdContloller));
router.delete('/loads/:id', authorizationMiddleware, asyncHelper(
    deleteLoadByIdContloller));
router.post('/loads/:id/post', authorizationMiddleware, asyncHelper(
    postLoadByIdContloller));
router.get('/loads/:id/shipping_info', authorizationMiddleware, asyncHelper(
    readShipingInfoByIdContloller));
module.exports = router;
