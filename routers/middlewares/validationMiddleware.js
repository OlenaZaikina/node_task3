const Joi = require('joi');

module.exports.validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .min(3)
        .max(64)
        .pattern(new RegExp(
            '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$')).required(),
    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{4,30}$')).required(),
    role: Joi.string()
        .min(5)
        .max(7)
        .required(),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.validateLogin = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .min(3)
        .max(64)
        .pattern(new RegExp(
            '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$')).required(),
    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{4,30}$')).required(),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.validateChangingPassword = async (req, res, next) => {
  const schema = Joi.object({
    oldPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{4,30}$')).required(),
    newPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{4,30}$')).required(),
  });

  await schema.validateAsync(req.body);
  next();
};
