const express = require('express');
const router = new express.Router();

const {asyncHelper} = require('./helper');
const {validateChangingPassword} = require(
    './middlewares/validationMiddleware');
const {authorizationMiddleware} = require('./middlewares/authMiddleware');
const {openProfileContloller, changePasswordContloller,
  deleteUserProfileContloller} = require('../controller/usersController');

router.get('/me', authorizationMiddleware, asyncHelper(openProfileContloller));
router.patch('/me/password', authorizationMiddleware, asyncHelper(
    validateChangingPassword), asyncHelper(changePasswordContloller));
router.delete('/me', authorizationMiddleware, asyncHelper(
    deleteUserProfileContloller));

module.exports = router;
