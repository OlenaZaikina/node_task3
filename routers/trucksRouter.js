const express = require('express');
const router = new express.Router();

const {asyncHelper} = require('./helper');
const {authorizationMiddleware} = require('./middlewares/authMiddleware');
const {createTruckContloller, readTruckByIdContloller, readTrucksContloller,
  updateTruckByIdContloller, deleteTruckByIdContloller,
  assignTruckByIdContloller} = require('../controller/truckController');
router.get('/trucks', authorizationMiddleware, asyncHelper(
    readTrucksContloller));
router.post('/trucks', authorizationMiddleware, asyncHelper(
    createTruckContloller));
router.get('/trucks/:id', authorizationMiddleware, asyncHelper(
    readTruckByIdContloller));
router.put('/trucks/:id', authorizationMiddleware, asyncHelper(
    updateTruckByIdContloller));
router.delete('/trucks/:id', authorizationMiddleware, asyncHelper(
    deleteTruckByIdContloller));
router.post('/trucks/:id/assign', authorizationMiddleware, asyncHelper(
    assignTruckByIdContloller));

module.exports = router;
