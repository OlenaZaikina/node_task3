const express = require('express');
const mongoose = require('mongoose');
const fs = require('fs');
const morgan = require('morgan');
const path = require('path');
const app = express();

const port = process.env.PORT || 8080;

const userRouter = require('./routers/userRouter');
const authRouter = require('./routers/authRouter');
const loadsRouter = require('./routers/loadsRouter');
const trucksRouter = require('./routers/trucksRouter');
const accessLogStream = fs.createWriteStream(path.join(__dirname, 'logger.log'),
    {flags: 'a'});

app.use(morgan('combined', {stream: accessLogStream}));
app.use(express.static('build'));
app.use(express.json());

app.use('/api/users', userRouter);
app.use('/api/auth', authRouter);
app.use('/api', loadsRouter);
app.use('/api', trucksRouter);

/**
 * Class representing a dot.
 * @extends Error
 */
class UnauthorizedError extends Error {
  /**
     * Create a dot.
     * @param {string} message - The message value.
     */
  constructor(message = 'Unauthorized user!') {
    super(message);
    statusCode = 400;
  }
}

app.use((err, req, res, next) => {
  if (err instanceof UnauthorizedError) {
    res.status(err.statusCode).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});

const connectDB = async () => {
  await mongoose.connect('mongodb+srv://testUser_123:testUser_123@nodemongolearning.xykp8.mongodb.net/hometask_3?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(port, function() {
    console.log(`Server is running on ${port} port`);
  });
};

connectDB();
