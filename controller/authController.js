const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');
const {RegistrationCredentials} = require(
    '../models/registrationCredentialsModel');
const {Credentials} = require('../models/credentialsModel');

module.exports.registrationContloller = async (req, res) => {
  const {email, password, role} = req.body;

  const registrationCredentials = new RegistrationCredentials({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });

  const credentials = new Credentials({
    email,
    password: await bcrypt.hash(password, 10),
  });

  const user = new User({
    email,
  });

  await credentials.save();
  await registrationCredentials.save();
  await user.save();
  res.json({message: 'Profile created successfully'});
};

module.exports.loginContloller = async (req, res) => {
  const {email, password} = req.body;
  console.log('password', password);

  const user = await Credentials.findOne({email});

  if (!user) {
    return res.status(400).json({message: `Wrong password or username`});
  }

  if ( !(await bcrypt.compare(password, user.password)) ) {
    return res.status(400).json({message: `Wrong password or username!`});
  }

  const token = jwt.sign(
      {email: user.email, _id: user._id}, JWT_SECRET);
  res.json({jwt_token: token});
};
