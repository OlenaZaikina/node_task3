const {Load} = require('../models/loadModel');
const {Truck} = require('../models/truckModel');
const {RegistrationCredentials} = require(
    '../models/registrationCredentialsModel');

const TRUCKS_LIST = {
  'SPRINTER': {
    'payload': 1700,
    'width': 300,
    'length': 250,
    'height': 170,
  },
  'SMALL STRAIGHT': {
    'payload': 2500,
    'width': 500,
    'length': 250,
    'height': 200,
  },
  'LARGE STRAIGHT': {
    'payload': 4000,
    'width': 700,
    'length': 350,
    'height': 200,
  },
};

module.exports.readLoadsContloller = async (req, res) => {
  const {_id, email} = req.user;
  let offset = parseInt(req.query.offset);
  let limit = parseInt(req.query.limit);

  const userRole = await RegistrationCredentials.findOne({email});

  if (userRole.role !== 'SHIPPER') {
    const loads = await Load.find({assigned_to: _id});
    console.log(loads);
    res.json({
      loads,
    });
  }

  if (!limit && limit < 1) limit = 10;
  else if (limit > 50) limit = 50;

  if (!offset && offset <= 0) offset = 0;

  const loads = await Load.find({created_by: _id}, {}, {skip: offset, limit});
  console.log(loads);

  if (!loads.length) {
    return res.status(200).json({message: `User has no notes`});
  }

  res.json({
    'loads': loads,
  });
};

module.exports.createLoadContloller = async (req, res) => {
  const {name, payload, pickup_address, delivery_address,
    dimensions} = req.body;
  const {_id, email} = req.user;

  const userRole = await RegistrationCredentials.findOne({email});

  if (userRole.role !== 'SHIPPER') {
    return res.status(400).json({message: `You are not shipper`});
  }

  const load = new Load({
    created_by: _id,
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  });
  await load.save();
  res.json({message: 'Load created successfully'});
};

module.exports.readLoadByIdContloller = async (req, res) => {
  console.log('readLoadByIdContloller');
  const loadId = req.params.id;
  const {_id} = req.user;
  const load = await Load.findOne({_id: loadId, created_by: _id});

  if (!load) {
    return res.status(400).json({message: `Load was not found`});
  }

  res.json({
    load,
  });
};

module.exports.updateLoadByIdContloller = async (req, res) => {
  const loadId = req.params.id;
  const {_id} = req.user;
  const {name, payload, pickup_address, delivery_address,
    dimensions} = req.body;

  const checkLoad = await Load.findOne({_id: loadId, created_by: _id}, {});
  const newStatus = await Load.findOne({_id: loadId, created_by: _id},
      {status: 1});

  if (!checkLoad) {
    return res.status(400).json({message: `Load does not exist`});
  }

  if (newStatus.status !== 'NEW') {
    return res.status(400).json({
      message: `You could change load only with 'NEW' status`});
  }

  await Load.updateOne({_id: loadId},
      {$set: {name, payload, pickup_address, delivery_address,
        dimensions}}, {});
  res.json({
    message: 'Load details changed successfully',
  });
};

module.exports.deleteLoadByIdContloller = async (req, res) => {
  const loadId = req.params.id;
  const {_id} = req.user;

  const checkLoad = await Load.findOne({_id: loadId, created_by: _id}, {});
  const newStatus = await Load.findOne({_id: loadId, created_by: _id},
      {status: 1});

  if (!checkLoad) {
    return res.status(400).json({message: `Load does not exist`});
  }

  if (newStatus.status !== 'NEW') {
    return res.status(400).json({
      message: `You could delete load only with 'NEW' status`,
    });
  }

  await Load.deleteOne({_id: loadId});
  res.json({
    'message': 'Load deleted successfully',
  });
};

module.exports.postLoadByIdContloller = async (req, res) => {
  const loadId = req.params.id;
  const {_id, email} = req.user;

  const userRole = await RegistrationCredentials.findOne({email});
  if (userRole.role !== 'SHIPPER') {
    return res.status(400).json({message: `You are not shipper`});
  }

  const load = await Load.findOne({_id: loadId, created_by: _id},
      {status: 1, payload: 1, dimensions: 1, logs: 1});
  await Load.updateOne({_id: loadId}, {$set: {status: 'POSTED'}}, {});

  const trucks = await Truck.find({}, {status: 1, assigned_to: 1, type: 1});

  const result = await findTruck(load, trucks, TRUCKS_LIST);

  if (!result) {
    await Load.updateOne({_id: loadId}, {$push: {logs: [{
      'message': 'Truck was not found',
      'time': new Date().toISOString().split('.')[0] + 'Z',
    }]}, $set: {status: 'NEW'}}, {});
    res.json({
      'message': 'Truck was not found',
    });
  } else {
    await Load.updateOne({_id: loadId}, {$push: {logs: [{
      'message': `Load assigned to driver with id ${result}`,
      'time': new Date().toISOString().split('.')[0] + 'Z',
    }]}, $set: {status: 'ASSIGNED', assigned_to: result}}, {});
    await Truck.updateOne({assigned_to: result}, {$set: {status: 'OL'}}, {});
    res.json({
      'message': 'Load posted successfully',
      'driver_found': true,
    });
  }
};

async function findTruck(load, trucks, TRUCKS_LIST) {
  const result = await findFitTruck(load, trucks, TRUCKS_LIST);
  return result;
};

function findFitTruck(load, trucks, TRUCKS_LIST) {
  let type = '';
  for (truck of trucks) {
    if (truck.assigned_to != '' && truck.status == 'IS') {
      type = truck.type;
      if (TRUCKS_LIST[type].payload > load.payload &&
        TRUCKS_LIST[type].width > load.dimensions.width &&
        TRUCKS_LIST[type].length > load.dimensions.length &&
        TRUCKS_LIST[type].height > load.dimensions.height) {
        result = truck.assigned_to;
        return result;
      }
    }
  }
}

module.exports.readShipingInfoByIdContloller = async (req, res) => {
  const loadId = req.params.id;
  const {_id} = req.user;
  const load = await Load.findOne({_id: loadId, created_by: _id});
  const loadDriver = await Load.findOne({_id: loadId, created_by: _id},
      {assigned_to: 1});
  const truck = await Truck.findOne({assigned_to: loadDriver.assigned_to,
    created_by: loadDriver.assigned_to});

  if (!load) {
    return res.status(400).json({message: `Load was not found`});
  }

  res.json({
    load,
    truck,
  });
};

module.exports.readActiveLoadContloller = async (req, res) => {
  const {_id} = req.user;
  const load = await Load.findOne({assigned_to: _id});

  if (!load) {
    return res.status(400).json({message: 'You do not have any load'});
  }

  res.json({
    load,
  });
};

module.exports.nextLoadStateContloller = async (req, res) => {
  const {_id} = req.user;
  const load = await Load.findOne({assigned_to: _id});
  if (!load) {
    return res.status(400).json({message: 'You do not have any load'});
  }
  const statesList = ['En route to Pick Up', 'Arrived to Pick Up',
    'En route to delivery', 'Arrived to delivery'];

  for (let st in statesList) {
    if (load.state === statesList[st] && st < 2) {
      st++;
      await Load.updateOne({assigned_to: _id},
          {$set: {state: statesList[st]}}, {});
      res.json({
        'message': `Load state changed to '${statesList[st]}'`,
      });
      return;
    } else if (st == 2) {
      st++;
      await Load.updateOne({assigned_to: _id},
          {$set: {state: statesList[st], status: 'SHIPPED'}}, {});
      res.json({
        'message': `Load state changed to '${statesList[st]}'`,
      });
    }
  }
};
