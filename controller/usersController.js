const {User} = require('../models/userModel');
const {Credentials} = require('../models/credentialsModel');
const {RegistrationCredentials} = require(
    '../models/registrationCredentialsModel');
const bcrypt = require('bcrypt');

module.exports.openProfileContloller = async (req, res) => {
  const {email} = req.user;
  const user = await User.findOne({email});

  if (!user) {
    return res.status(400).json({message: `User was not found`});
  }

  res.json({
    'user': {
      '_id': user._id,
      'email': user.email,
      'createdDate': user.createdDate,
    },
  });
};

module.exports.changePasswordContloller = async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const {email} = req.user;

  const dbPassword = await Credentials.findOne({email});

  if ( !(await bcrypt.compare(oldPassword, dbPassword.password)) ) {
    return res.status(400).json({message: `Wrong password`});
  }

  if (oldPassword === newPassword) {
    return res.status(400).json({message: `The same password`});
  }
  const hashedPass = await bcrypt.hash(newPassword, 10);

  await Credentials.updateOne(
      {email: email}, {$set: {password: hashedPass}}, {});

  await RegistrationCredentials.updateOne(
      {email: email}, {$set: {password: hashedPass}}, {});

  res.json({
    'message': 'Password changed successfully',
  });
};

module.exports.deleteUserProfileContloller = async (req, res) => {
  const {_id, email} = req.user;
  const user = await User.find({_id});

  if (!user) {
    return res.status(400).json({message: `User does not exist`});
  }

  /*   await Note.deleteMany({userId: _id}); */
  await User.deleteMany({email});
  await Credentials.deleteMany({email});
  await RegistrationCredentials.deleteMany({email});

  res.json({
    'message': 'Profile deleted successfully',
  });
};
