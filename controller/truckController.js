const {Truck} = require('../models/truckModel');
const {RegistrationCredentials} = require(
    '../models/registrationCredentialsModel');

module.exports.readTrucksContloller = async (req, res) => {
  const {_id} = req.user;

  const trucks = await Truck.find({created_by: _id});

  if (!trucks.length) {
    return res.status(400).json({message: `Trucks was not found`});
  }

  res.json({
    trucks,
  });
};

module.exports.createTruckContloller = async (req, res) => {
  const {type} = req.body;
  const {_id, email} = req.user;

  const userRole = await RegistrationCredentials.findOne({email});

  if (userRole.role !== 'DRIVER') {
    return res.status(400).json({message: `You are not driver`});
  }

  const truck = new Truck({
    created_by: _id,
    type,
  });
  await truck.save();
  res.json({message: 'Truck created successfully'});
};

module.exports.readTruckByIdContloller = async (req, res) => {
  const _id = req.params.id;

  const truck = await Truck.findOne({_id});

  if (!truck) {
    return res.status(400).json({message: `Truck was not found`});
  }

  res.json({
    truck,
  });
};

module.exports.updateTruckByIdContloller = async (req, res) => {
  const truckId = req.params.id;
  const {_id} = req.user;
  const {type} = req.body;

  const checkTruck = await Truck.findOne({_id: truckId, created_by: _id}, {});
  const assigned = await Truck.findOne({_id: truckId,
    created_by: _id}, {assigned_to: 1});

  if (!checkTruck) {
    return res.status(400).json({message: `Truck does not exist`});
  }

  if (assigned.assigned_to === _id) {
    return res.status(400).json({
      message: `You couldn't change assigned truck`,
    });
  }

  await Truck.updateOne({_id: truckId}, {$set: {type}}, {});
  res.json({
    message: 'Truck details changed successfully',
  });
};

module.exports.deleteTruckByIdContloller = async (req, res) => {
  const truckId = req.params.id;
  const {_id} = req.user;

  const checkTruck = await Truck.findOne({_id: truckId,
    created_by: _id}, {});
  const assigned = await Truck.findOne({_id: truckId,
    created_by: _id}, {assigned_to: 1});

  if (!checkTruck) {
    return res.status(400).json({message: `Truck does not exist`});
  }

  if (assigned.assigned_to === _id) {
    return res.status(400).json({
      message: `You couldn't delete assigned truck`,
    });
  }

  await Truck.deleteOne({_id: truckId});
  res.json({
    'message': 'Truck deleted successfully',
  });
};

module.exports.assignTruckByIdContloller = async (req, res) => {
  const truckId = req.params.id;
  const {_id, email} = req.user;

  const userRole = await RegistrationCredentials.findOne({email});

  if (userRole.role !== 'DRIVER') {
    return res.status(400).json({message: `You are not driver`});
  }

  const trucks = await Truck.find({created_by: _id}, {assigned_to: 1});
  for (const truck of trucks) {
    if (truck.assigned_to === _id) {
      return res.status(400).json({
        message: `You have already had an assigned truck`,
      });
    }
  }

  await Truck.updateOne({_id: truckId}, {$set: {assigned_to: _id}}, {});
  res.json({
    message: 'Truck assigned successfully',
  });
};
